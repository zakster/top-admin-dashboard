# top-admin-dashboard

Project: Admin Dashboard

https://www.theodinproject.com/lessons/node-path-intermediate-html-and-css-admin-dashboard

Tools used:
- [Boxicons](https://boxicons.com/) 
- [Coollabs](https://coollabs.io/) 
- [Svgrepo](https://www.svgrepo.com/) 
- [Tailwind Colors](https://tailwindcss.com/docs/customizing-colors) 
